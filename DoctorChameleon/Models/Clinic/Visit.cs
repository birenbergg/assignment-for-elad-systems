﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoctorChameleon.Models.Clinic
{
    public class Visit
    {
        public int VisitId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime Date { get; set; }

        public string Diagnosis { get; set; }

        public int PatientId { get; set; }
        public Patient Patient { get; set; }

        public virtual ICollection<Medication> Medications { get; set; }
    }
}