﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DoctorChameleon.Data;
using DoctorChameleon.Models.Clinic;
using DoctorChameleon.ViewModels;
using System.Data.Entity.Infrastructure;

namespace DoctorChameleon.Controllers
{
    public class VisitsController : Controller
    {
        private ClinicContext db = new ClinicContext();

        // GET: Visits
        public ActionResult Index()
        {
            var visits = db.Visits.Include(v => v.Patient);
            return View(visits.ToList());
        }

        // GET: Visits/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Visit visit = db.Visits.Include(v => v.Patient).Where(v => v.VisitId == id).Single();
            if (visit == null)
            {
                return HttpNotFound();
            }
            PopulatePrescribedMedications(visit);
            return View(visit);
        }

        // GET: Visits/Create
        public ActionResult Create(int? patientId)
        {
            ViewBag.PatientId = new SelectList(db.Patients, "PatientId", "Name", patientId);
            var visit = new Visit();
            visit.Medications = new List<Medication>();
            PopulatePrescribedMedications(visit);

            return View();
        }

        // POST: Visits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VisitId,Date,Diagnosis,PatientId")] Visit visit, string[] selectedMeds)
        {
            if (selectedMeds != null)
            {
                visit.Medications = new List<Medication>();

                foreach (var med in selectedMeds)
                {
                    var medToAdd = db.Medications.Find(int.Parse(med));
                    visit.Medications.Add(medToAdd);
                }
            }

            if (ModelState.IsValid)
            {
                db.Visits.Add(visit);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PatientId = new SelectList(db.Patients, "PatientId", "Name", visit.PatientId);
            PopulatePrescribedMedications(visit);
            return View(visit);
        }

        // GET: Visits/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Visit visit = db.Visits.Find(id);
            if (visit == null)
            {
                return HttpNotFound();
            }
            ViewBag.PatientId = new SelectList(db.Patients, "PatientId", "Name", visit.PatientId);
            PopulatePrescribedMedications(visit);
            return View(visit);
        }

        // POST: Visits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, string[] selectedMeds)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Visit visit = db.Visits.Include(v => v.Patient).Where(v => v.VisitId == id).Single();

            if (TryUpdateModel(visit, "", new string[] { "VisitId", "Date", "Diagnosis", "PatientId" }))
            {
                try
                {
                    UpdateMeds(selectedMeds, visit);

                    db.Entry(visit).State = EntityState.Modified;
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Unable to save changes.");
                }
            }

            PopulatePrescribedMedications(visit);

            ViewBag.PatientId = new SelectList(db.Patients, "PatientId", "Name", visit.PatientId);
            return View(visit);
        }

        private void UpdateMeds(string[] selectedMeds, Visit visit)
        {
            if (selectedMeds == null)
            {
                visit.Medications = new List<Medication>();
                return;
            }

            var selectedMedsHS = new HashSet<string>(selectedMeds);
            var visitMeds = new HashSet<int>(visit.Medications.Select(m => m.MedicationId));

            foreach (var med in db.Medications)
            {
                if (selectedMedsHS.Contains(med.MedicationId.ToString()))
                {
                    if (!visitMeds.Contains(med.MedicationId))
                    {
                        visit.Medications.Add(med);
                    }
                }
                else
                {
                    if (visitMeds.Contains(med.MedicationId))
                    {
                        visit.Medications.Remove(med);
                    }
                }
            }
        }

        // GET: Visits/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Visit visit = db.Visits.Include(v => v.Patient).Where(v => v.VisitId == id).Single();
            if (visit == null)
            {
                return HttpNotFound();
            }
            return View(visit);
        }

        // POST: Visits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Visit visit = db.Visits.Find(id);
            db.Visits.Remove(visit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private void PopulatePrescribedMedications(Visit visit)
        {
            var allMedications = db.Medications;
            var prescribedMedications = new HashSet<int>(visit.Medications.Select(m => m.MedicationId));
            var viewModel = new List<VisitMedicationsVM>();

            foreach (var medication in allMedications)
            {
                viewModel.Add(new VisitMedicationsVM
                {
                    MedicationId = medication.MedicationId,
                    Name = medication.Name,
                    Prescribed = prescribedMedications.Contains(medication.MedicationId)
                });
                ViewBag.Medications = viewModel;
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
