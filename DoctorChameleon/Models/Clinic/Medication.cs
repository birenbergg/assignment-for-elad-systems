﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoctorChameleon.Models.Clinic
{
    public class Medication
    {
        public int MedicationId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Visit> Visits { get; set; }
    }
}