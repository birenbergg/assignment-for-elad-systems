namespace DoctorChameleon.Migrations.Clinic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Medications",
                c => new
                    {
                        MedicationId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MedicationId);
            
            CreateTable(
                "dbo.Visits",
                c => new
                    {
                        VisitId = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Diagnosis = c.String(),
                        PatientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.VisitId)
                .ForeignKey("dbo.Patients", t => t.PatientId, cascadeDelete: true)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        PatientId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.PatientId);
            
            CreateTable(
                "dbo.VisitMedications",
                c => new
                    {
                        Visit_VisitId = c.Int(nullable: false),
                        Medication_MedicationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Visit_VisitId, t.Medication_MedicationId })
                .ForeignKey("dbo.Visits", t => t.Visit_VisitId, cascadeDelete: true)
                .ForeignKey("dbo.Medications", t => t.Medication_MedicationId, cascadeDelete: true)
                .Index(t => t.Visit_VisitId)
                .Index(t => t.Medication_MedicationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Visits", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.VisitMedications", "Medication_MedicationId", "dbo.Medications");
            DropForeignKey("dbo.VisitMedications", "Visit_VisitId", "dbo.Visits");
            DropIndex("dbo.VisitMedications", new[] { "Medication_MedicationId" });
            DropIndex("dbo.VisitMedications", new[] { "Visit_VisitId" });
            DropIndex("dbo.Visits", new[] { "PatientId" });
            DropTable("dbo.VisitMedications");
            DropTable("dbo.Patients");
            DropTable("dbo.Visits");
            DropTable("dbo.Medications");
        }
    }
}
