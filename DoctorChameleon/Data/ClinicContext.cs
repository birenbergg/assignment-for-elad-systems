﻿using DoctorChameleon.Models.Clinic;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DoctorChameleon.Data
{
    public class ClinicContext : DbContext
    {
        public ClinicContext() : base("DefaultConnection") { }

        public DbSet<Patient> Patients { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<Medication> Medications { get; set; }
    }
}