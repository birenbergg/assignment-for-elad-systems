﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DoctorChameleon.Startup))]
namespace DoctorChameleon
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
