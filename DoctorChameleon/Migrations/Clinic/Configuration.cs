namespace DoctorChameleon.Migrations.Clinic
{
    using DoctorChameleon.Data;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DoctorChameleon.Data.ClinicContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;

            MigrationsDirectory = @"Migrations\Clinic";
        }

        protected override void Seed(DoctorChameleon.Data.ClinicContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            //context.Patients.AddOrUpdate(p => p.PatientId, DummyData.getPatients().ToArray());
            //context.SaveChanges();
            //context.Visits.AddOrUpdate(v => v.VisitId, DummyData.getVisits(context).ToArray());
            //context.SaveChanges();
            //context.Medications.AddOrUpdate(m => m.MedicationId, DummyData.getMedications(context).ToArray());
            //context.SaveChanges();
        }
    }
}
