﻿using DoctorChameleon.Models.Clinic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoctorChameleon.Data
{
    public class DummyData
    {
        public static List<Patient> getPatients()
        {
            List<Patient> patients = new List<Patient>
            {
                new Patient
                {
                    FirstName = "Ron",
                    LastName = "Gilbert"
                },
                new Patient
                {
                    FirstName = "Gary",
                    LastName = "Winnick"
                },
                new Patient
                {
                    FirstName = "David",
                    LastName = "Fox"
                }
            };

            return patients;
        }

        public static List<Visit> getVisits(ClinicContext context)
        {
            List<Visit> visits = new List<Visit>
            {
                new Visit
                {
                    Date = new DateTime(2015, 5, 15),
                    Diagnosis = "Earring Sign - when the ear lobes are open from having their earrings pulled out; usually indicates a psych background.",
                    PatientId = context.Patients.Find(1).PatientId
                },
                new Visit
                {
                    Date = new DateTime(2016, 2, 11),
                    Diagnosis = "Polysuggestivitis - patients that do what you say they should have done; for example, telling a patient when people have seizures, they always flail their left arm and the left arm begins to flail.",
                    PatientId = context.Patients.Find(1).PatientId
                },
                new Visit
                {
                    Date = new DateTime(2015, 12, 10),
                    Diagnosis = "To Hell And Back -  seen when patients and their family are healthcare trained.",
                    PatientId = context.Patients.Find(2).PatientId
                },
                new Visit
                {
                    Date = new DateTime(2016, 4, 1),
                    Diagnosis = "Too Demented To Care About Paying For A Nursing Home But The Daughter Wants The Farmland -  keeping mom home alone without supervision for farmland reasons.",
                    PatientId = context.Patients.Find(2).PatientId
                },
                new Visit
                {
                    Date = new DateTime(2016, 8, 29),
                    Diagnosis = "Insurance Pain -  pain that presents when insurance settlements are involved.",
                    PatientId = context.Patients.Find(2).PatientId
                }
            };

            return visits;
        }

        public static List<Medication> getMedications(ClinicContext context)
        {
            List<Medication> medications = new List<Medication>
            {
                new Medication
                {
                    Name = "Elixir of life"
                },
                new Medication
                {
                    Name = "Elixir of long life"
                },
                new Medication
                {
                    Name = "Paracetamoxyfrusebendroneomycin"
                },
                new Medication
                {
                    Name = "Unnamed potion of deathlike sleep"
                },
                new Medication
                {
                    Name = "Spaceoline"
                },
                new Medication
                {
                    Name = "Air"
                },
                new Medication
                {
                    Name = "Betaphenethylamine"
                },
                new Medication
                {
                    Name = "Red dye #2"
                }
            };

            return medications;
        }
    }
}