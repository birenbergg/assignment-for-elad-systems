﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoctorChameleon.ViewModels
{
    public class VisitMedicationsVM
    {
        public int MedicationId { get; set; }
        public string Name { get; set; }
        public bool Prescribed { get; set; }
    }
}